Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0024-camera-3d).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         |                  |
| assembly drawing         |                  |
| circuit diagram          |                  |
| maintenance instructions |                  |
| spare parts              |                  |

